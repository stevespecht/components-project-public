﻿/*
    File: WordHelper.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: instance of word application for determaning if played words are real words
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;

namespace BoggleLibrary
{
    public class WordHelper
    {
        public static Word.Application wordApp = new Word.Application();


        public static void CloseWord()
        {
            //Marshal.FinalReleaseComObject(wordApp);
            wordApp.Quit();
        }
    }
}
