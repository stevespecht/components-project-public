﻿/*
    File: BoggleGame.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: library for boggle  
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;


namespace BoggleLibrary
{
    public interface ICallback
    {
        [OperationContract(IsOneWay = true)]
        void Update();

        [OperationContract(IsOneWay = true)]
        void SetStart(bool status);

        [OperationContract(IsOneWay = true)]
        void StartClocks(int seconds);
    }

    //interface to the boggle game class
    [ServiceContract(CallbackContract = typeof(ICallback))]
    public interface IBoggleGame
    {
        //Operation Contracts for boggle
        [OperationContract]
        int GetPlayersCount();

        [OperationContract]
        Player GetPlayer(int playerId);

        [OperationContract]
        int JoinGame();

        [OperationContract]
        void ReadyUp(int playerId);

        [OperationContract]
        void StartRound();

        [OperationContract(IsOneWay = true)]
        void EndRound();
        
        [OperationContract(IsOneWay = true)]
        void LeaveGame(int playerId);

        [OperationContract]
        List<string> GetDice();

        [OperationContract]
        int GetJoined();

        [OperationContract]
        int GetReady();

        [OperationContract]
        void SubmitWord(int playerId, List<string> words);

        [OperationContract]
        void SetName(int playerId, string name);

        [OperationContract]
        bool GetShowScoreboard();

        [OperationContract]
        void SetShowScoreboard(bool value);

        [OperationContract]
        List<Player> GetPlayerScores();

        [OperationContract]
        void NotReady(int playerId);
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class BoggleGame : IBoggleGame
    {
        public static Dictionary<Player, ICallback> Players { get; set; }

        public static  List<Player> PlayerScores = new List<Player>();

        private int JoinedCount { get; set; }

        static private int ReadyCount { get; set; }

        static private bool ShowScoreboard { get; set; } = false;

        private enum GameStatus { Join, ReadyUp, Playing };

        private GameStatus currentStatus = GameStatus.Join;

        //Name: BoggleGame
        //Input: N/A
        //Ouput: N/A
        //Purpose: default constructor for boggle game
        public BoggleGame()
        {
            Players = new Dictionary<Player, ICallback>();
        }

        ~BoggleGame() { }

        //Name: JoinGame
        //Input: N/A
        //Ouput: int player id 
        //Purpose: join player to game
        public int JoinGame()
        {
            ICallback cb = OperationContext.Current.GetCallbackChannel<ICallback>();

            if (currentStatus != GameStatus.Playing)
            {
                Player newPlayer = new Player();
                Players.Add(newPlayer, cb);

                Console.WriteLine($"Player {newPlayer.Id} joined");

                JoinedCount++;

                updateAllPlayers();

                if (currentStatus == GameStatus.ReadyUp)
                    setStartButton(false);

                return newPlayer.Id;
            }

            else
            {
                Player newPlayer = new Player();
                Console.WriteLine($"Game in progress new Player Joined queue {newPlayer.Id}");
                return newPlayer.Id;
            }
        }

        //Name: ReadyUp
        //Input: int of player Id
        //Ouput: void 
        //Purpose: set the player status to ready
        public void ReadyUp(int playerId)
        {
            ReadyCount++;
            updateAllPlayers();

            Console.WriteLine($"Player {playerId} ready");

            GetPlayer(playerId).PlayerStatus = Player.Status.Ready;

            CheckAllReady();       
        }

        //Name: CheckAllReady
        //Input: N/A
        //Ouput: void 
        //Purpose: check if all players are ready, if they are start the round
        public void CheckAllReady()
        {
            bool allPlayersRdy = Players.checkAllReady();

            if (allPlayersRdy)
            {
                setStartButton(true);
                currentStatus = GameStatus.ReadyUp;
            }
        }

        //Name: NotReady
        //Input: int of player Id
        //Ouput: void 
        //Purpose: set player status to not ready
        public void NotReady(int playerId)
        {
            if (ReadyCount > 0)
            {
                ReadyCount--;
            }     
            
            updateAllPlayers();

            Console.WriteLine($"Player {playerId} is not ready");

            GetPlayer(playerId).PlayerStatus = Player.Status.NotReady;
        }

        //Name: StartRound
        //Input: N/A
        //Ouput: void 
        //Purpose: start the round
        public int GetPlayersCount()
        {
            return Players.Count;
        }

        //Name: StartRound
        //Input: N/A
        //Ouput: void 
        //Purpose: start the round
        public void StartRound()
        {
            currentStatus = GameStatus.Playing;
            updateAllPlayers();
            startTheClocks();
        }

        //Name: EndRound
        //Input: N/A
        //Ouput: void 
        //Purpose: end the round
        public void EndRound()
        {
            updateAllPlayers();
        }

        //Name: LeaveGame
        //Input: int of player id
        //Ouput: void 
        //Purpose: player with the same id leaves game
        public void LeaveGame(int playerId)
        {
            ICallback cb = OperationContext.Current.GetCallbackChannel<ICallback>();

            // Need to test this
            if (Players.ContainsValue(cb))
            {
                Players.Remove(GetPlayer(playerId));
            }

            Console.WriteLine($"Player {playerId} left the game");
            if(JoinedCount > 0)
                JoinedCount--;
            if(ReadyCount > 0)
                ReadyCount--;
            updateAllPlayers();
            CheckAllReady();
        }

        //Name: GetPlayer
        //Input: int of player id
        //Ouput: Player 
        //Purpose: gets player with the same id
        public Player GetPlayer(int playerId)
        {
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                if (player.Key.Id == playerId)
                    return player.Key;
            }
            return null;
        }

        //Name: setStartButton
        //Input: bool of status 
        //Ouput: void
        //Purpose: all players set start
        private void setStartButton(bool status)
        {
            foreach (ICallback cb in Players.Values)
            {
                cb.SetStart(status);
            }
        }

        //Name: updateAllPlayers
        //Input: N/A
        //Ouput: void
        //Purpose: all players update
        static private void updateAllPlayers()
        {
            foreach (ICallback cb in Players.Values)
            {
                cb.Update();
            }
        }

        //Name: startTheClocks
        //Input: N/A
        //Ouput: void
        //Purpose: all clients start clocks
        private void startTheClocks()
        {
            foreach (ICallback cb in Players.Values)
            {
                cb.StartClocks(5);
            }
        }

        //Name: GetJoined
        //Input: N/A
        //Ouput: int JoinedCount
        //Purpose: getter for JoinedCount
        public int GetJoined()
        {
            return JoinedCount;
        }

        //Name: GetReady
        //Input: N/A
        //Ouput: int ReadyCount
        //Purpose: getter for ReadyCount
        public int GetReady()
        {
            return ReadyCount;
        }

        //List of the rolled dice
        private static List<string> rolledDice = new List<string>();

        //Name: GetDice
        //Input: N/A
        //Ouput: List<string> of rolledDice
        //Purpose: getter for rolledDice
        public List<string> GetDice()
        {
            return rolledDice;
        }

        //Name: ShakeBoard
        //Input: N/A
        //Ouput: void
        //Purpose: shake board to get new letters
        public static void ShakeBoard()
        {
            List<string[]> dice = new List<string[]>();

            dice.Add(new string[] { "B", "F", "I", "O", "R", "X" });
            dice.Add(new string[] { "F", "E", "E", "H", "I", "Y" });
            dice.Add(new string[] { "D", "E", "N", "O", "S", "W" });
            dice.Add(new string[] { "D", "K", "N", "O", "T", "U" });
            dice.Add(new string[] { "A", "H", "M", "O", "S", "R" });
            dice.Add(new string[] { "E", "L", "P", "S", "T", "U" });
            dice.Add(new string[] { "A", "A", "C", "I", "O", "T" });
            dice.Add(new string[] { "E", "G", "K", "L", "U", "Y" });
            dice.Add(new string[] { "A", "B", "J", "M", "O", "Qu" });
            dice.Add(new string[] { "E", "H", "I", "N", "S", "P" });
            dice.Add(new string[] { "E", "G", "I", "N", "T", "V" });
            dice.Add(new string[] { "A", "B", "I", "L", "T", "Y" });
            dice.Add(new string[] { "A", "D", "E", "N", "V", "Z" });
            dice.Add(new string[] { "A", "C", "E", "L", "R", "S" });
            dice.Add(new string[] { "G", "I", "L", "R", "U", "W" });
            dice.Add(new string[] { "A", "C", "D", "E", "P", "M" });

            Random randomFace = new Random();

            for (int i = 0; i < dice.Count; i++)
            {
                rolledDice.Add(dice[i][randomFace.Next(0, 6)]);
            }

            rolledDice.Shuffle();
        }

        //Name: SubmitWord
        //Input: int of playerId, List<string> of words
        //Ouput: void
        //Purpose: submit played words
        public void SubmitWord(int playerId, List<string> words)
        {
            Player currentPlayer = GetPlayer(playerId);

            currentPlayer.Words = words;
            currentPlayer.AreWordsSubmitted = true;

            CheckAllSubmitted();
            Console.WriteLine($"Player {playerId} submitted their words");
        }

        //Name: CheckAllSubmitted
        //Input: N/A
        //Ouput: void
        //Purpose: check if all players have submitted their words at the end of a round
        void CheckAllSubmitted()
        {
            bool allPlayersSubmitted = true;

            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                if (player.Key.AreWordsSubmitted == false)
                {
                    allPlayersSubmitted = false;
                    break;
                }
            }

            if (allPlayersSubmitted)
            {
                Players.resetWordSubmittedBool();
                Console.WriteLine($"All players have submitted their words");
                EvaluateWords();
            }
        }

        //Name: IncreaseRound
        //Input: N/A
        //Ouput: void
        //Purpose: evaluate played words at the end of a round, shows scoreboard, starts a new round
        static void EvaluateWords()
        {
            IncreaseRound();
            RemoveDuplicates();
            ValidateWord();
            ScoreWords();
            CalculateAvg();
            UpdatePlayerScores();
            ShowScoreboard = true;
            updateAllPlayers();
            NewRound();
        }

        //Name: IncreaseRound
        //Input: N/A
        //Ouput: void
        //Purpose: increase the round for each player
        static void IncreaseRound()
        {
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                player.Key.Round++;
            }
        }

        //Name: ValidateWord
        //Input: N/A
        //Ouput: void
        //Purpose: determin which unique played words are valid words for all players
        static void ValidateWord()
        {
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                for (int i = 0; i < player.Key.UniqueWords.Count; i++)
                {
                    if (IsWord(player.Key.UniqueWords[i]))
                    {
                        player.Key.ValidWords.Add(player.Key.UniqueWords[i]);
                    }
                }
            }
        }

        //Name: IsWord
        //Input: string of potential word
        //Ouput: bool stating if word
        //Purpose: determin if candidate word is actually a word according to word dictionary
        static bool IsWord(string candidate)
        {
            bool result = false;

            if (WordHelper.wordApp.CheckSpelling(candidate.ToLower()))
            {
                result = true;
            }

            return result;
        }

        //Name: RemoveDuplicates
        //Input: N/A
        //Ouput: void
        //Purpose: remove duplicate words from your own played words and your opnents
        static void RemoveDuplicates()
        {
            //Remove duplicates from within
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                player.Key.Words = player.Key.Words.Distinct().ToList();
            }

            List<string> masterList = new List<string>();

            //Add to master list
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                masterList.AddRange(player.Key.Words);
            }

            //Remove duplicates
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                for (int j = 0; j < player.Key.Words.Count; j++)
                {
                    if (!(masterList.FindAll(f => f.Equals(player.Key.Words[j])).Count > 1))
                    {
                        player.Key.UniqueWords.Add(player.Key.Words[j]);
                    }

                    else
                    {
                        player.Key.NonUniqueWords.Add(player.Key.Words[j]);
                    }
                }
            }
        }

        //Name: ScoreWords
        //Input: N/A
        //Ouput: void
        //Purpose: calculate the score for each player
        static void ScoreWords()
        {
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                int totalScore = 0;

                for (int i = 0; i < player.Key.ValidWords.Count; i++)
                {
                    if (player.Key.ValidWords[i].Length == 3 || player.Key.ValidWords[i].Length == 4)
                    {
                        totalScore += 1;
                    }

                    else if (player.Key.ValidWords[i].Length == 5)
                    {
                        totalScore += 2;
                    }

                    else if (player.Key.ValidWords[i].Length == 6)
                    {
                        totalScore += 3;
                    }

                    else if (player.Key.ValidWords[i].Length == 7)
                    {
                        totalScore += 5;
                    }

                    else if (player.Key.ValidWords[i].Length >= 8)
                    {
                        totalScore += 11;
                    }
                }

                player.Key.TotalScore += totalScore;
                player.Key.Score = totalScore; 
            }
        }

        //Name: CalculateAvg
        //Input: N/A
        //Ouput: void
        //Purpose: calculate the avg score per round for each player
        static void CalculateAvg()
        {
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                player.Key.AvgScore = player.Key.Score / player.Key.Round;
            }
        }

        //Name: SetName
        //Input: int of playerId, string of name
        //Ouput: void
        //Purpose: set the player name for the player with the same id
        public void SetName(int playerId, string name)
        {
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                if (player.Key.Id == playerId)
                {
                    player.Key.Name = name;
                }
            }
        }

        //Name: UpdatePlayerScores
        //Input: N/A 
        //Ouput: void
        //Purpose: add all players to PlayerScores
        static void UpdatePlayerScores()
        {
            PlayerScores = new List<Player>();

            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                PlayerScores.Add((Player)player.Key.Clone());
            }
        }

        //Name: GetPlayerScores
        //Input: N/A 
        //Ouput: List<Player> of PlayerScores
        //Purpose: getter for PlayerScores
        public List<Player> GetPlayerScores()
        {
            return PlayerScores;
        }

        //Name: SetShowScoreboard
        //Input: bool 
        //Ouput: void
        //Purpose: setter for show scoreboard
        public bool GetShowScoreboard()
        {
            return ShowScoreboard;
        }

        //Name: SetShowScoreboard
        //Input: bool 
        //Ouput: void
        //Purpose: setter for show scoreboard
        public void SetShowScoreboard(bool value)
        {
            ShowScoreboard = value;
        }

        //Name: NewRound
        //Input: N/A
        //Ouput: void
        //Purpose: set up a new round service side
        static public void NewRound()
        {
            rolledDice.Clear();

            ShakeBoard();

            ReadyCount = 0;

            //set all players to not ready
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                player.Key.PlayerStatus = Player.Status.NotReady;
            }

            Players.resetWords();
        }
    }    
}
