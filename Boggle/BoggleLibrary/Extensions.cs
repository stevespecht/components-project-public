﻿/*
    File: Extension.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: extension class
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoggleLibrary
{
    static class Extensions
    {
        //Name: Shuffle
        //Input: this list<T> of dice
        //Ouput: void
        //Purpose: shuffle the order of the letter dice, does not select or change the letters
        public static void Shuffle<T>(this IList<T> dice)
        {
            Random random = new Random();

            int index = dice.Count;
            int rngIndex = 0;

            while (index > 1)
            {
                index--;
                rngIndex = random.Next(index + 1);

                T tempLetter = dice[rngIndex];
                dice[rngIndex] = dice[index];
                dice[index] = tempLetter;
            }
        }

        //Name: resetWordSubmittedBool
        //Input: this Dictionary<Player, ICallback> of players
        //Ouput: void
        //Purpose: reset AreWordsSubmitted for all players
        public static void resetWordSubmittedBool(this Dictionary<Player, ICallback> players)
        {
            foreach(KeyValuePair<Player, ICallback> player  in players)
            {
                player.Key.AreWordsSubmitted = false;
            }
        }

        //Name: resetWords
        //Input: this Dictionary<Player, ICallback> of players
        //Ouput: void
        //Purpose: reset all words lists for all players
        public static void resetWords(this Dictionary<Player, ICallback> players)
        {
            foreach (KeyValuePair<Player, ICallback> player in players)
            {
                player.Key.Words = new List<string>();
                player.Key.NonUniqueWords = new List<string>();
                player.Key.UniqueWords = new List<string>();
                player.Key.ValidWords = new List<string>();                
            }
        }

        //Name: resetWords
        //Input: this Dictionary<Player, ICallback> of players
        //Ouput: void
        //Purpose: check if all players are ready
        public static bool checkAllReady(this Dictionary<Player, ICallback> Players)
        {
            bool allPlayersRdy = true;
            foreach (KeyValuePair<Player, ICallback> player in Players)
            {
                if (player.Key.PlayerStatus == Player.Status.NotReady)
                {
                    allPlayersRdy = false;
                    break;
                }
            }
            return allPlayersRdy;
        }
    }
}
