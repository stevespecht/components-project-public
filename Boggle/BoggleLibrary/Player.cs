﻿/*
    File: Player.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: player class for boggle 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;

namespace BoggleLibrary
{
    [DataContract]
    public class Player : ICloneable
    {
        public enum Status { NotReady, Ready};

        //getters and setters for data members
        [DataMember] public int Id { get; set; }

        [DataMember] public int Score { get; set; }

        [DataMember] public List<string> Words { get; set; }

        [DataMember] public Status PlayerStatus { get; set; }

        [DataMember] public List<string> NonUniqueWords { get; set; }

        [DataMember] public List<string> UniqueWords { get; set; }

        [DataMember] public List<string> ValidWords { get; set; }

        [DataMember] public bool AreWordsSubmitted { get; set; }

        [DataMember] public int TotalScore { get; set; } = 0;

        [DataMember] public string Name { get; set; }

        [DataMember] public int Round { get; set; }

        [DataMember] public double AvgScore { get; set; }

        public static int id = 0;

        //Name: Player
        //Input: N/A
        //Ouput: N/A 
        //Purpose: default constructor for player
        public Player()
        {
            Id = id++;
            Words = new List<string>();
            NonUniqueWords = new List<string>();
            UniqueWords = new List<string>();
            ValidWords = new List<string>();
            Score = 0;
            PlayerStatus = Status.NotReady;
        }

        //clone object, to avoid pass by ref
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}

