﻿/*
    File: Program.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: main program for boggle, sets up service, shakes board
*/

using System;
using System.ServiceModel;
using BoggleLibrary;

namespace BoggleService
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost servHost = null;
            try
            {
                // Address
                servHost = new ServiceHost(typeof(BoggleGame));
                servHost.Open();
                Console.WriteLine("Service started. Press a key to quit.");
                BoggleGame.ShakeBoard();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            finally
            {
               
                Console.ReadKey();
                WordHelper.CloseWord();
                if (servHost != null)
                    servHost.Close();
            }
        }
    }
}
