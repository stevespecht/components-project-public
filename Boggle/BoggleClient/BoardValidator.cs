﻿/*
    File: BoardValidator.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: Used for evaluating valid boggle "moves", which letters you can select
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoggleClient
{
    //Extension class
    public static class TwoDArrayExtensions
    {
        public static void SetArray(this bool[,] a, bool val)
        {
            for (int i = a.GetLowerBound(0); i <= a.GetUpperBound(0); i++)
            {
                for (int j = a.GetLowerBound(1); j <= a.GetUpperBound(1); j++)
                {
                    a[i, j] = val;
                }
            }
        }
    }

    public class boggleDie
    {
        public string boggleString = "";
        public char boggleChar= ' ';

        //Name: boggleDie 2 arg constructor
        //Input: string of boggleString, string of boggleChar
        //Ouput: N/A 
        //Purpose: initialize letter die on the boggle board 
        public boggleDie(string bStr, char bChar)
        {
            boggleString = bStr;
            boggleChar = bChar;
        }
    }

    //Validate the word to only allow for valid boggle moves,
    //horizontal, vertical and diagonal letters that are directly touching the last latter played
    /*Board validator trie solution was modified from this example
     * https://www.geeksforgeeks.org/boggle-set-2-using-trie/
     */
    class BoardValidator
    {
        const int BOARDSIZE = 4;
        public static boggleDie[,] boggleBoard = new boggleDie[BOARDSIZE, BOARDSIZE];
        static bool[,]  visited = new bool[BOARDSIZE, BOARDSIZE];

        static readonly int SIZE = 26;
     
        // trie Node  
        public class TrieNode
        {
            public TrieNode[] Child = new TrieNode[SIZE];

            public string nodeValue = "";
            // isLeaf is true if the node represents  
            // end of a word  
            public bool leaf;

            //constructor  
            public TrieNode()
            {
                leaf = false;
                nodeValue = "";
                for (int i = 0; i < SIZE; i++)
                    Child[i] = null;
            }
        }

        static void insert(TrieNode root, String Key)
        {
            //int n = Key.Length;
            TrieNode pChild = root;

            char prev = ' ';
            foreach (char i in Key)
            {
                if (prev == 'Q')
                {
                    prev = ' ';
                    continue;
                }
                if (pChild.Child[i - 'A'] == null)
                {
                    pChild.Child[i - 'A'] = new TrieNode();
                }

                pChild = pChild.Child[i - 'A'];
                if (i == 'Q')
                {
                    prev = 'Q';
                }
            }

            // make last node as leaf node  
            pChild.leaf = true;
        }

        // function to check that current location  
        // (i and j) is in matrix range  
        static bool isSafe(int i, int j)
        {
            return (i >= 0 && i < BOARDSIZE && j >= 0 &&
                    j < BOARDSIZE && !visited[i, j]);
        }

        // A recursive function to print all words present on boggle  
        static bool searchWord(TrieNode root, int i, int j, String str)
        {
            // if we found word in trie / dictionary  
            if (root.leaf == true)
                return true;

            if (isSafe(i, j))
            {
                // make it visited  
                visited[i, j] = true;

                // traverse all child of current root  
                //foreach(TrieNode tri in root.Child)
                for (int K = 0; K < SIZE; K++)
                {
                    if (root.Child[K] != null)
                    {
                        // current character  
                        char ch = (char)(K + 'A');

                        int iUp = i + 1;
                        int iDown = i - 1;
                        int jUp = j + 1;
                        int jDown = j - 1;

                        //This section has been modified to support Qu from dice the orignal example only supported Q
                        if (isSafe(iUp, jUp) && boggleBoard[iUp, jUp].boggleChar == ch)
                            if (searchWord(root.Child[K], iUp, jUp, str + boggleBoard[iUp, jUp].boggleString))
                                return true;
                        if (isSafe(i, jUp) && boggleBoard[i, jUp].boggleChar == ch)
                            if (searchWord(root.Child[K], i, jUp, str + boggleBoard[i, jUp].boggleString))
                                return true;
                        if (isSafe(iDown, jUp) && boggleBoard[iDown, jUp].boggleChar == ch)
                            if (searchWord(root.Child[K], iDown, jUp, str + boggleBoard[iDown, jUp].boggleString))
                                return true;
                        if (isSafe(iUp, j) && boggleBoard[iUp, j].boggleChar == ch)
                            if (searchWord(root.Child[K], iUp, j, str + boggleBoard[iUp, j].boggleString))
                                return true;
                        if (isSafe(iUp, jDown) && boggleBoard[iUp, jDown].boggleChar == ch)
                            if (searchWord(root.Child[K], iUp, jDown, str + boggleBoard[iUp, jDown].boggleString))
                                return true;
                        if (isSafe(i, jDown) && boggleBoard[i, jDown].boggleChar == ch)
                            if (searchWord(root.Child[K], i, jDown, str + boggleBoard[i, jDown].boggleString))
                                return true;
                        if (isSafe(iDown, jDown) && boggleBoard[iDown, jDown].boggleChar == ch)
                            if (searchWord(root.Child[K], iDown, jDown, str + boggleBoard[iDown, jDown].boggleString))
                                return true;
                        if (isSafe(iDown, j) && boggleBoard[iDown, j].boggleChar == ch)
                            if (searchWord(root.Child[K], iDown, j, str + boggleBoard[iDown, j].boggleString))
                                return true;
                    }
                }

                // make current element unvisited  
                visited[i, j] = false;
            }
            return false;
        }

        // Prints all words present in dictionary.  
        static bool findWords(TrieNode root)
        {
            // Mark all characters as not visited  
            visited.SetArray(false);
            TrieNode pChild = root;

            String str = "";

            // traverse all matrix elements  
            for (int i = 0; i < BOARDSIZE; i++)
            {
                for (int j = 0; j < BOARDSIZE; j++)
                {
                    //check input root string
                    if (pChild.Child[(boggleBoard[i, j]).boggleChar - 'A'] != null)
                    {
                        str = str + boggleBoard[i, j].boggleString;
                        if (searchWord(pChild.Child[(boggleBoard[i, j]).boggleChar - 'A'],
                                i, j, str))
                            return true;
                        else
                            str = "";
                    }
                }
            }
            return false;
        }

        public static bool findwithTrie(string input)
        {
            TrieNode root = new TrieNode();

            insert(root, input);

           return findWords(root);
        }
    }
}

