﻿/*
    File: MainWindow.xaml.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: code behinde for the mainwindow, the boggle board
*/

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using System.ServiceModel;
using BoggleLibrary;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace BoggleClient
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public partial class MainWindow : Window, ICallback
    {
        DuplexChannelFactory<IBoggleGame> channel;
        IBoggleGame game;
        int playerId;
        DispatcherTimer timer;
        TimeSpan time;
        string playerName = "";

        //MainWindow Constructor
        public MainWindow()
        {
            //join service
            channel = new DuplexChannelFactory<IBoggleGame>(this, "BoggleGameService");
            game = channel.CreateChannel();
            playerId = game.JoinGame();

            InitializeComponent();

            //initialize playedwords list and list box for played words
            PlayedWords = new ObservableCollection<string>();
            lbPlayedWords.ItemsSource = PlayedWords;

            //set letter die to '?'
            btnCell1.Content = "?";
            btnCell2.Content = "?";
            btnCell3.Content = "?";
            btnCell4.Content = "?";
            btnCell5.Content = "?";
            btnCell6.Content = "?";
            btnCell7.Content = "?";
            btnCell8.Content = "?";
            btnCell9.Content = "?";
            btnCell10.Content = "?";
            btnCell11.Content = "?";
            btnCell12.Content = "?";
            btnCell13.Content = "?";
            btnCell14.Content = "?";
            btnCell15.Content = "?";
            btnCell16.Content = "?";

            //diable letter die clicks
            btnCell1.IsEnabled = false;
            btnCell2.IsEnabled = false;
            btnCell3.IsEnabled = false;
            btnCell4.IsEnabled = false;
            btnCell5.IsEnabled = false;
            btnCell6.IsEnabled = false;
            btnCell7.IsEnabled = false;
            btnCell8.IsEnabled = false;
            btnCell9.IsEnabled = false;
            btnCell10.IsEnabled = false;
            btnCell11.IsEnabled = false;
            btnCell12.IsEnabled = false;
            btnCell13.IsEnabled = false;
            btnCell14.IsEnabled = false;
            btnCell15.IsEnabled = false;
            btnCell16.IsEnabled = false;

            //disable game buttons
            btnReady.IsEnabled = false;
            btnPlay.IsEnabled = false;
            btnRemove.IsEnabled = false;
            btnClear.IsEnabled = false;
        }

        ObservableCollection<string> PlayedWords { get; set; }

        //Name: btnPlayClicked
        //Input: button click event args
        //Ouput: void
        //Purpose: handle play button click, play word
        private void btnPlayClicked(object sender, RoutedEventArgs e)
        {
            PlayWord();
        }

        //Name: btnRemoveClicked
        //Input: button click event args
        //Ouput: void
        //Purpose: handle remove button click, remove last played word
        private void tbCurrentWordEnterKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                PlayWord();
            }            
        }


        //Name: btnRemoveClicked
        //Input: button click event args
        //Ouput: void
        //Purpose: handle remove button click, remove last played word
        private void btnRemoveClicked(object sender, RoutedEventArgs e)
        {
            if (PlayedWords.Count > 0)
            {
                PlayedWords.RemoveAt(PlayedWords.Count - 1);
            }

            if (PlayedWords.Count == 0)
            {
                btnRemove.IsEnabled = false;
            }
        }

        //Name: btnClearClicked
        //Input: button click event args
        //Ouput: void
        //Purpose: handle clear button click, clear current word
        private void btnClearClicked(object sender, RoutedEventArgs e)
        {
            tbCurrentWord.Text = "";
        }

        //Name: btnLetterClicked
        //Input: button click event args
        //Ouput: void
        //Purpose: handle letter click, put clicked letter in current word textbox
        private void btnLetterClicked(object sender, RoutedEventArgs e)
        {
            tbCurrentWord.Text += (e.Source as Button).Content.ToString();
        }

        //Name: ShakeBoard
        //Input: N/A
        //Ouput: void
        //Purpose: get shaken letters from service and assign letters to letter die, enable letter die
        void ShakeBoard()
        {
            //get shaken letters from service
            List<string> dice = game.GetDice();
            btnCell1.Content = dice[0];
            btnCell2.Content = dice[1];
            btnCell3.Content = dice[2];
            btnCell4.Content = dice[3];
            btnCell5.Content = dice[4];
            btnCell6.Content = dice[5];
            btnCell7.Content = dice[6];
            btnCell8.Content = dice[7];
            btnCell9.Content = dice[8];
            btnCell10.Content = dice[9];
            btnCell11.Content = dice[10];
            btnCell12.Content = dice[11];
            btnCell13.Content = dice[12];
            btnCell14.Content = dice[13];
            btnCell15.Content = dice[14];
            btnCell16.Content = dice[15];

            //enable letter die
            btnCell1.IsEnabled = true;
            btnCell2.IsEnabled = true;
            btnCell3.IsEnabled = true;
            btnCell4.IsEnabled = true;
            btnCell5.IsEnabled = true;
            btnCell6.IsEnabled = true;
            btnCell7.IsEnabled = true;
            btnCell8.IsEnabled = true;
            btnCell9.IsEnabled = true;
            btnCell10.IsEnabled = true;
            btnCell11.IsEnabled = true;
            btnCell12.IsEnabled = true;
            btnCell13.IsEnabled = true;
            btnCell14.IsEnabled = true;
            btnCell15.IsEnabled = true;
            btnCell16.IsEnabled = true;

            //Assign dice to the boggleBoard 2d array
            int k = 0;
            for (int i =0; i< 4; ++i)
            {
                for(int j=0; j < 4; ++j)
                {
                    BoardValidator.boggleBoard[i, j] = new boggleDie(dice[k], dice[k].Equals("Qu") ? 'Q' : char.Parse(dice[k]));
                    ++k;
                }
            }
            Console.WriteLine("shake board done");
        }

        //Name: PlayWord
        //Input: N/A
        //Ouput: void
        //Purpose: play word if there is a word to play
        void PlayWord()
        {
            if (tbCurrentWord.Text != "")
            {
                PlayedWords.Add(tbCurrentWord.Text.ToUpper());
                tbCurrentWord.Text = "";
                btnRemove.IsEnabled = true;
            }

            SetTextBoxFocus();
        }

        //Name: EndGame
        //Input: N/A
        //Ouput: void
        //Purpose: end game, submit played words
        void EndGame()
        {
            game.SubmitWord(playerId, new List<string>(PlayedWords));
        }

        //Name: SetTextBoxFocus
        //Input: N/A
        //Ouput: void
        //Purpose: set focus on current word textbox
        void SetTextBoxFocus()
        {
            tbCurrentWord.Focusable = true;
            Keyboard.Focus(tbCurrentWord);
        }

        //Name: SetTime
        //Input: int in minutes
        //Ouput: void
        //Purpose: set game timer, once 0 stop time and end game
        void SetTime(int minutes)
        {
            time = TimeSpan.FromMinutes(minutes);

            timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                tbTimer.Text = string.Format("{0:D2}:{1:D2}", (int)time.TotalMinutes, time.Seconds);

                if (time == TimeSpan.Zero)
                {
                    timer.Stop();

                    EndGame();                    
                }

                time = time.Add(TimeSpan.FromSeconds(-1));

            }, Application.Current.Dispatcher);

            timer.Start();
        }

        //Name: GameStartCountDown
        //Input: int in seconds
        //Ouput: void
        //Purpose: set countdown timer, once 0 stop time and start game by starting game time and shaking board
        void GameStartCountDown(int seconds)
        {
            time = TimeSpan.FromSeconds(seconds);

            timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                tbTimer.Text = string.Format("{0:D2}:{1:D2}", (int)time.TotalMinutes, time.Seconds);

                if (time == TimeSpan.Zero)
                {
                    timer.Stop();

                    SetTime(3);
                    ShakeBoard();
                }

                time = time.Add(TimeSpan.FromSeconds(-1));

            }, Application.Current.Dispatcher);

            timer.Start();
        }

        //Name: updateControls
        //Input: N/A
        //Ouput: void
        //Purpose: set joined status text, handle scoreboard and new round
        private void updateControls()
        {
            tbPlayersJoined.Text = game.GetJoined().ToString();
            tbPlayersReady.Text = game.GetReady().ToString();

            if (game.GetShowScoreboard())
            {
                ShowScoreboard(game.GetPlayerScores());
                game.SetShowScoreboard(false);
                NewRound();
            }
        }

        //Name: EnableStartGame
        //Input: bool of button status
        //Ouput: void
        //Purpose: enable/disable start button 
        private void EnableStartGame(bool status)
        {
            btnStartGame.IsEnabled = status;
        }

        private delegate void GuiUpdateDelegate();
        private delegate void StartGameButtonDelegate(bool status);
        private delegate void StartGameCountDown(int seconds);

        //Name: Update
        //Input: N/A
        //Ouput: void
        //Purpose: handle update calls from service
        public void Update()
        {
            if (this.Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                try
                {
                    updateControls();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(new GuiUpdateDelegate(Update));
            }
        }

        //Name: SetStart
        //Input: bool of start button status
        //Ouput: void
        //Purpose: handle start status calls from service
        public void SetStart(bool status)
        {
            if (this.Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                try
                {
                    EnableStartGame(status);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(new StartGameButtonDelegate(SetStart), status);
            }
        }

        //Name: SetStart
        //Input: int in seconds
        //Ouput: void
        //Purpose: handle start clock calls from service
        public void StartClocks(int seconds)
        {
            if (this.Dispatcher.Thread == System.Threading.Thread.CurrentThread)
            {
                try
                {
                    btnStartGame.IsEnabled = false;
                    btnReady.IsEnabled = false;
                    tbName.IsEnabled = false;
                    GameStartCountDown(seconds);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                this.Dispatcher.BeginInvoke(new StartGameCountDown(StartClocks), seconds);
            }
        }

        //Name: Window_Closing
        //Input: close event args
        //Ouput: void
        //Purpose: when client closes player leaves game
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            game.LeaveGame(playerId);
        }

        bool IsReady { get; set; } = true;

        //Name: BtnReady_Click
        //Input: button event args
        //Ouput: void
        //Purpose: handle ready button click, toggle ready or not ready
        private void BtnReady_Click(object sender, RoutedEventArgs e)
        {
            if (IsReady)
            {
                game.SetName(playerId, playerName);

                btnReady.Content = "Not Ready";
                tbTimer.Text = "Ready";
                game.ReadyUp(playerId);

                IsReady = false;
            }

            else
            {
                game.NotReady(playerId);
                btnReady.Content = "Ready to Play";
                tbTimer.Text = "Not Ready";
                IsReady = true;
            }
        }

        //Name: BtnStartGame_Click
        //Input: button event args
        //Ouput: void
        //Purpose: handle start game button click, start game
        private void BtnStartGame_Click(object sender, RoutedEventArgs e)
        {
            game.StartRound();
        }

        //Name: TbCurrentWord_TextChanged
        //Input: text box event args
        //Ouput: void
        //Purpose: handle current word text box changes, validate word
        private void TbCurrentWord_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbCurrentWord.Text != "")
            {
                if (Regex.IsMatch(tbCurrentWord.Text, @"([A-Za-z])$", RegexOptions.IgnoreCase))
                {
                    if (tbCurrentWord.Text.Substring(tbCurrentWord.Text.Length - 1) == "Q" || tbCurrentWord.Text.Substring(tbCurrentWord.Text.Length - 1) == "q")
                    {
                        tbCurrentWord.Text += "u";
                        tbCurrentWord.CaretIndex = tbCurrentWord.Text.Length;
                    }

                    if (!BoardValidator.findwithTrie(tbCurrentWord.Text.ToUpper()))
                    {
                        tbCurrentWord.Text = tbCurrentWord.Text.Substring(0, tbCurrentWord.Text.Length - 1);
                        tbCurrentWord.CaretIndex = tbCurrentWord.Text.Length;
                    }
                }
                else
                {
                    tbCurrentWord.Text = tbCurrentWord.Text.Substring(0, tbCurrentWord.Text.Length - 1);
                    tbCurrentWord.CaretIndex = tbCurrentWord.Text.Length;
                }

                btnPlay.IsEnabled = true;
                btnClear.IsEnabled = true;
            }

            else
            {
                btnPlay.IsEnabled = false;
                btnClear.IsEnabled = false;
            }
        }

        //Name: ShowScoreboard
        //Input: List<Player> of players
        //Ouput: void
        //Purpose: show scoreboard
        void ShowScoreboard(List<Player> PlayerScores)
        {
            ScoreboardWindow scoreboardWindow = new ScoreboardWindow(playerId, PlayerScores);
            scoreboardWindow.ShowDialog();
        }

        //Name: TextBox_TextChanged
        //Input: text box event args
        //Ouput: void
        //Purpose: handle name text box changes
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            playerName = tbName.Text;

            if (playerName != "")
            {
                btnReady.IsEnabled = true;
            }

            else
            {
                btnReady.IsEnabled = false;
            }
        }

        //Name: NewRound
        //Input: N/A
        //Ouput: void
        //Purpose: start a new round
        void NewRound()
        {
            //reset ready elements
            IsReady = true;
            btnReady.Content = "Ready to Play";
            tbTimer.Text = "Not Ready";

            //reset buttons, clear words
            tbName.IsEnabled = true;
            btnReady.IsEnabled = true;
            btnStartGame.IsEnabled = false;
            btnPlay.IsEnabled = false;
            btnRemove.IsEnabled = false;
            btnClear.IsEnabled = false;
            PlayedWords.Clear();

            //set letter die to '?'
            btnCell1.Content = "?";
            btnCell2.Content = "?";
            btnCell3.Content = "?";
            btnCell4.Content = "?";
            btnCell5.Content = "?";
            btnCell6.Content = "?";
            btnCell7.Content = "?";
            btnCell8.Content = "?";
            btnCell9.Content = "?";
            btnCell10.Content = "?";
            btnCell11.Content = "?";
            btnCell12.Content = "?";
            btnCell13.Content = "?";
            btnCell14.Content = "?";
            btnCell15.Content = "?";
            btnCell16.Content = "?";

            //disable letter die
            btnCell1.IsEnabled = false;
            btnCell2.IsEnabled = false;
            btnCell3.IsEnabled = false;
            btnCell4.IsEnabled = false;
            btnCell5.IsEnabled = false;
            btnCell6.IsEnabled = false;
            btnCell7.IsEnabled = false;
            btnCell8.IsEnabled = false;
            btnCell9.IsEnabled = false;
            btnCell10.IsEnabled = false;
            btnCell11.IsEnabled = false;
            btnCell12.IsEnabled = false;
            btnCell13.IsEnabled = false;
            btnCell14.IsEnabled = false;
            btnCell15.IsEnabled = false;
            btnCell16.IsEnabled = false;
        }
    }
}
