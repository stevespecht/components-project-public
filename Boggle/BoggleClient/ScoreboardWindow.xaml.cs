﻿/*
    File: ScoreboardWindow.xaml.cs
    Name: John Herring, Steven Specht
    Date: 08/04/2019
    Purpose: code behinde for the ScoreboardWindow, the scoreboard
*/

using System.Collections.Generic;
using System.Windows;
using BoggleLibrary;

namespace BoggleClient
{
    /// <summary>
    /// Interaction logic for ScoreboardWindow.xaml
    /// </summary>
    public partial class ScoreboardWindow : Window
    {
        //dep prop for score board
        public List<Player> PlayerScores
        {
            get { return (List<Player>)GetValue(PlayerScoresProperty); }
            set { SetValue(PlayerScoresProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PlayerScores.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlayerScoresProperty =
            DependencyProperty.Register("PlayerScores", typeof(List<Player>), typeof(ScoreboardWindow), new PropertyMetadata());


        //dep prop for your round results
        public List<Player> MyRoundResults
        {
            get { return (List<Player>)GetValue(MyRoundResultsProperty); }
            set { SetValue(MyRoundResultsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MyRoundResults.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MyRoundResultsProperty =
            DependencyProperty.Register("MyRoundResults", typeof(List<Player>), typeof(ScoreboardWindow), new PropertyMetadata());

        //Name: ScoreboardWindow 2 arg contructor
        //Input: int of player id, List<Player> of plyer info
        //Ouput: void
        //Purpose: build scoreboard
        public ScoreboardWindow(int playerId, List<Player> newScores)
        {
            InitializeComponent();
            DataContext = this;

            //assign scores to scoreboard
            PlayerScores = newScores;

            //assign your word info to round results
            foreach (Player player in newScores)
            {
                if (playerId == player.Id)
                {
                    MyRoundResults = new List<Player> { player };
                    break;
                }
            }
        }
    }    
}
